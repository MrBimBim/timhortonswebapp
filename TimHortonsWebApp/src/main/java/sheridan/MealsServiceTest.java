package sheridan;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MealsServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDrinkRegular() {
	MealsService mealservice = new MealsService();
	List<String> meals = mealservice.getAvailableMealTypes(MealType.DRINKS);
	assertTrue("MealType Drinks is not empty", meals.isEmpty() == false);
	
	}
	@Test
	public void testDrinkException() {
		MealsService mealservice = new MealsService();
		List<String> meals = mealservice.getAvailableMealTypes(null);
		assertTrue("MealType gives No Brand available ", meals.get(0).equals("No Brand Available"));
		
	}
	@Test
	public void testDrinkBoundaryIn() {
		MealsService mealservice = new MealsService();
		List<String> meals = mealservice.getAvailableMealTypes(MealType.DRINKS);
		assertTrue("MealType Drinks has more than 3", meals.size() > 3);
		
	}
	@Test
	public void testDrinkRegularBoundaryOut() {
		MealsService mealservice = new MealsService();
		List<String> meals = mealservice.getAvailableMealTypes(null);
		assertTrue("MealType gives only 1 element", meals.size() == 1);
		
	}

}
